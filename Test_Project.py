import random

def get_number_input(prompt):
    """Safely get an integer input from the user."""
    while True:
        try:
            return int(input(prompt))
        except ValueError:
            print("请输入一个有效的整数。")

def choose_difficulty():
    """Let the user choose a difficulty level."""
    difficulty = input("请选择难度等级（简单/中等/困难）: ").lower()
    if difficulty == "简单":
        return 1, 50  # 简单难度的范围
    elif difficulty == "中等":
        return 1, 100  # 中等难度的范围
    elif difficulty == "困难":
        return 1, 200  # 困难难度的范围
    else:
        print("未识别的难度等级，将使用默认难度：中等。")
        return 1, 100  # 默认难度

def calculate_max_attempts(min_val, max_val):
    """Dynamically calculate the max attempts based on the range."""
    range_size = max_val - min_val + 1
    return min(10, max(3, int(range_size ** 0.5)))  # 最少3次，最多10次尝试机会

def play_game(min_val, max_val):
    """Play a single game round."""
    secret_number = random.randint(min_val, max_val)
    max_attempts = calculate_max_attempts(min_val, max_val)
    print(f"猜测数字范围在 {min_val} 到 {max_val} 之间。你有 {max_attempts} 次尝试机会。")

    attempts = 0
    guesses = []
    while attempts < max_attempts:
        guess = get_number_input("你的猜测: ")
        attempts += 1
        guesses.append(guess)

        if guess < min_val or guess > max_val:
            print(f"数字必须在 {min_val} 到 {max_val} 之间。")
        elif guess < secret_number:
            print("太低了！")
        elif guess > secret_number:
            print("太高了！")
        else:
            print(f"正确！你在 {attempts} 次尝试后猜对了数字。")
            break
    else:
        print(f"很遗憾，你的尝试次数已用完。数字是 {secret_number}。")
    print("你的猜测是:", guesses)

def main():
    print("欢迎来到随机数猜测游戏！")
    game_count = 0
    total_attempts = 0

    while True:
        min_val, max_val = choose_difficulty()  # 让用户选择难度等级
        play_game(min_val, max_val)
        game_count += 1
        play_again = input("再玩一次吗？(是/否): ").lower()
        if play_again != "是":
            break

    print(f"感谢参与游戏！你总共玩了 {game_count} 次。")

if __name__ == "__main__":
    main()
